import dotenv from 'dotenv';
dotenv.config();

const config = {
    PORT: process.env.PORT,
    BASE_URL_MODO: process.env.BASE_URL_MODO,
    STORE_ID: process.env.STORE_ID,
    CLIENT_ID: process.env.CLIENT_ID,
    CLIENT_SECRET: process.env.CLIENT_SECRET,
    APP_NAME: "MODO"
}

export default config;