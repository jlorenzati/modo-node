import axios from 'axios';
import config from '../config/config.js';

const generateAccessToken = async () => {
    console.log("generando access token");
    const response = await axios.post(`${process.env.BASE_URL_MODO}/merchants/middleman/token`,
        {
            username: config.CLIENT_ID,
            password: config.CLIENT_SECRET,
        },
        { headers: { 'Content-Type': 'application/json' } });
    console.log("genero access token");
    return response.data.accessToken;
};

export {generateAccessToken};