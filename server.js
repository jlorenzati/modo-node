import config from './config/config.js';
import app from './index.js'

const port = config.PORT;

async function init() {
	app.listen(port || 4000, () => {
        console.log(`Server running in port ${port}`);
    });

}

init();