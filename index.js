import express from 'express';
import morgan from 'morgan';
import config from './config/config.js';
import { createPaymentIntention } from './services/payment.js';

const app = express();

//Settings
app.set("appName", config.APP_NAME);
app.use(express.json()); //para que entienda el json

//Middlewares
app.use(morgan("dev"));

app.use(express.static("public"));

// MODO

app.post("/api/modo-checkout", async (req, res) => {
  res.json(await createPaymentIntention(req));
});


export default app;
