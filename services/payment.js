import axios from 'axios';
import config from '../config/config.js';
import {generateAccessToken} from '../helpers/accessToken.js';

// Create Payment Intention
const createPaymentIntention = async (req) => {
    console.log("generando payment intention");

    // Crear orden de compra en la base de datos de la tienda
    const mockOrder = {
      id: 123,
    };
  
    const accessToken = await generateAccessToken();
  
    const response = await axios.post(`${config.BASE_URL_MODO}/merchants/ecommerce/payment-intention`,
      {
        productName: 'Producto botón de pago',
        price: req.body.price,
        quantity: 1,
        storeId: config.STORE_ID,
        currency: 'ARS',
        externalIntentionId: mockOrder.id,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${accessToken}`,
        },
      });
    console.log("genero payment intention");
    return response.data;
};

export {createPaymentIntention};